import javax.swing.*;
import java.util.*;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class Main {
    private static String user_input;
    private static int inputted_value_int;
    private static HashSet<Integer> hashset = new HashSet<Integer>();
    private static List<Integer> print_list = new LinkedList<>();
    private static StringBuilder stringBuilder = new StringBuilder();

    public static void main(String[] args) {

        do {
            user_input = JOptionPane.showInputDialog(null, "請輸入一個正整數", "列出所有有3或是3之倍數的數字", JOptionPane.PLAIN_MESSAGE);

            try {

                inputted_value_int = Integer.parseInt(user_input);

                for (int i = 1; i <= inputted_value_int; i++){
                    if (i % 3 == 0 || String.valueOf(i).contains("3")) { //數字是含有3 / 3的倍數才記錄

                        hashset.add(i); //記錄在hashset

                    }
                }

                //用iterator 存取HashSet
                Iterator<Integer> iterator = hashset.iterator();

                while (iterator.hasNext()){ //檢查還有沒有下一個data

                    print_list.add(iterator.next()); //把data 放到Linked list裡面

                }
                //

                Collections.sort(print_list); //由小至大 排序data

                for (int j = 0; j < print_list.size(); j++) {

                    stringBuilder.append(print_list.get(j).toString());

                    if (j + 1 != print_list.size()) { //檢查不是最後一筆data 才執行下一行（因最後一筆data不需要 加上逗號 及 空格）
                        stringBuilder.append(", "); //data之間 加上逗號 及 空格
                    }
                }

                if (hashset.isEmpty()){

                    JOptionPane.showMessageDialog(null, "空空如也 QQ", user_input, JOptionPane.PLAIN_MESSAGE);

                }else {

                    JOptionPane.showMessageDialog(null, stringBuilder, user_input, JOptionPane.PLAIN_MESSAGE);

                }

            }catch (NumberFormatException ex){

                System.out.println("Cancel pressed / String inputted. Input has been discard.");
                System.exit(0);

            }

            //清除所有暫存data，避免二次執行時會顯示上一次的data
            hashset.clear();
            print_list.clear();
            stringBuilder.setLength(0);
            //

        }while (user_input != null); //null 只會出現在 1.沒輸入東西 2.按取消 3.按關閉視窗 才出現

    }
}
